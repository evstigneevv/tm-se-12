package ru.evstigneev.tm.exception;

public class RepositoryException extends Exception {

    public RepositoryException(String message) {
        super(message);
    }

    public RepositoryException() {
        super("Object is null!");
    }

}
