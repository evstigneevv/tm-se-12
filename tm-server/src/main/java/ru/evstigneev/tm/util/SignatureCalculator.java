package ru.evstigneev.tm.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.exception.EmptyStringException;

public class SignatureCalculator {

    public static String calculateSignature(@NotNull final Object value) throws Exception {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String valueString = objectMapper.writeValueAsString(value);
        return calculateSignature(valueString);
    }

    private static String calculateSignature(@NotNull final String value) throws Exception {
        if (value.isEmpty()) {
            throw new EmptyStringException();
        }
        @NotNull String result = value;
        for (int i = 0; i < 7; i++) {
            result = PasswordParser.getPasswordHash("three letter word" + result + "three letter word");
        }
        return result;
    }

}
