package ru.evstigneev.tm.util;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordParser {

    public static String getPasswordHash(String passwordInput) {
        @NotNull byte[] array = new byte[0];
        try {
            array = MessageDigest.getInstance("MD5").digest(passwordInput.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        @NotNull StringBuilder stringBuilder = new StringBuilder();
        for (byte oneByte : array) {
            stringBuilder.append(Integer.toHexString((oneByte & 0xFF) | 0x100));
        }
        return stringBuilder.toString();
    }

}
