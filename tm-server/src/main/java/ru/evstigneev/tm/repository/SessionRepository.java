package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.repository.ISessionRepository;
import ru.evstigneev.tm.entity.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(Connection connection) {
        super(connection);
    }

    public boolean persist(@NotNull final Session session) throws SQLException {
        @NotNull final String query = "insert into sessions values(?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getRoleType().displayName());
        statement.setLong(4, session.getTimestamp());
        statement.setString(5, session.getSignature());
        return statement.execute();
    }

    @Override
    public boolean removeByUserId(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "delete from sessions where user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        return statement.execute();
    }

}
