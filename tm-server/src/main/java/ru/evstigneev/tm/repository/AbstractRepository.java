package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public abstract class AbstractRepository<V> {

    Connection connection;

    public AbstractRepository() {
    }

    public AbstractRepository(Connection connection) {
        this.connection = connection;
    }

    public abstract boolean persist(@NotNull final V value) throws Exception;

}
