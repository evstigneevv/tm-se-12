package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.ITaskRepository;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String TASK_ID = "id";
    @NotNull
    private static final String USER_ID = "user_id";
    @NotNull
    private static final String PROJECT_ID = "project_id";
    @NotNull
    private static final String NAME = "name";
    @NotNull
    private static final String DESCRIPTION = "description";
    @NotNull
    private static final String DATE_OF_CREATION = "date_of_creation";
    @NotNull
    private static final String DATE_START = "date_start";
    @NotNull
    private static final String DATE_FINISH = "date_finish";
    @NotNull
    private static final String STATUS = "status";

    public TaskRepository(Connection connection) {
        super(connection);
    }

    @Override
    public boolean create(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name,
                          @NotNull final String description) throws SQLException {
        @NotNull final String query = "insert into tasks(id, user_id, project_id, name, description, date_of_creation," +
                " status) values(?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, UUID.randomUUID().toString());
        statement.setString(2, userId);
        statement.setString(3, projectId);
        statement.setString(4, name);
        statement.setString(5, description);
        statement.setDate(6, new java.sql.Date(System.currentTimeMillis()));
        statement.setString(7, Status.PLANNING.displayName());
        return statement.execute();
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String taskId) throws SQLException {
        @NotNull final String query = "delete from tasks where id = ? and user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, taskId);
        statement.setString(2, userId);
        return statement.execute();
    }

    @Override
    public boolean update(@NotNull final String userId, @NotNull final String taskId,
                          @NotNull final String name, @NotNull final String description,
                          @Nullable final Date dateStart, @Nullable final Date dateFinish, @NotNull final Status status) throws SQLException {
        @NotNull final String query = "update tasks set name = ?, description = ?, date_start = ?, date_finish = " +
                "?, status = ? where id = ? and user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setDate(3, dateStart == null ? null : new java.sql.Date(dateStart.getTime()));
        statement.setDate(4, dateFinish == null ? null : new java.sql.Date(dateFinish.getTime()));
        statement.setString(5, status.displayName());
        statement.setString(6, taskId);
        statement.setString(7, userId);
        return statement.executeUpdate() > 0;
    }

    @Override
    public Collection<Task> getTaskListByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        @NotNull final String query = "select id, user_id, project_id, name, description, date_of_creation, " +
                "date_start, date_finish, status from projects where user_id = ? and project_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        return fetchAll(statement.executeQuery());
    }

    @Override
    public Collection<Task> findAll() throws SQLException {
        @NotNull final String query = "select id, user_id , project_id, name, description, date_of_creation, " +
                "date_start, date_finish, status from tasks";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        return fetchAll(statement.executeQuery());
    }

    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "select id, user_id, project_id, name, description, date_of_creation, " +
                "date_start, date_finish, status from projects where user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        return fetchAll(statement.executeQuery());
    }

    @Override
    public boolean deleteAllProjectTasks(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        @NotNull final String query = "delete from tasks where user_id = ? and project_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        return statement.execute();
    }

    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws SQLException {
        @NotNull final String query = "select id, user_id, project_id, name, description, date_of_creation, " +
                "date_start, date_finish, status from projects where id = ? and user_id = ? and project_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, taskId);
        statement.setString(2, userId);
        statement.setString(3, projectId);
        return fetch(statement.executeQuery());
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "delete from tasks";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.execute();
    }

    @Override
    public boolean removeAllByUserId(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "delete from tasks where user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        return statement.execute();
    }

    @Override
    public boolean merge(@NotNull final String userId, @NotNull final Task task) throws SQLException {
        @NotNull final Task foundTask = findOne(userId, task.getProjectId(), task.getId());
        if (foundTask != null) {
            return update(foundTask.getUserId(), foundTask.getId(), foundTask.getName(),
                    foundTask.getDescription(), foundTask.getDateStart(), foundTask.getDateFinish(),
                    foundTask.getStatus());
        }
        return persist(task);
    }

    @Override
    public boolean persist(@NotNull final Task task) throws SQLException {
        @NotNull final String query = "insert into projects values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getUserId());
        statement.setString(3, task.getProjectId());
        statement.setString(4, task.getName());
        statement.setString(5, task.getDescription());
        statement.setDate(6, new java.sql.Date(task.getDateOfCreation().getTime()));
        statement.setDate(3, task.getDateStart() == null ? null : new java.sql.Date(task.getDateStart().getTime()));
        statement.setDate(4, task.getDateFinish() == null ? null : new java.sql.Date(task.getDateFinish().getTime()));
        statement.setString(9, task.getStatus().displayName());
        return statement.execute();
    }

    @Nullable
    private Task fetch(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        if (resultSet.next()) {
            @NotNull final Task task = new Task();
            task.setId(resultSet.getString(TASK_ID));
            task.setUserId(resultSet.getString(USER_ID));
            task.setProjectId(resultSet.getString(PROJECT_ID));
            task.setName(resultSet.getString(NAME));
            task.setDescription(resultSet.getString(DESCRIPTION));
            task.setDateOfCreation(resultSet.getDate(DATE_OF_CREATION));
            task.setDateStart(resultSet.getDate(DATE_START));
            task.setDateFinish(resultSet.getDate(DATE_FINISH));
            task.setStatus(Status.valueOf(resultSet.getString(STATUS)));
            return task;
        }
        return null;
    }

    @Nullable
    private Collection<Task> fetchAll(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        @NotNull final Collection<Task> tasks = new ArrayList<>();
        while (resultSet.next()) {
            @NotNull final Task task = new Task();
            task.setId(resultSet.getString(TASK_ID));
            task.setUserId(resultSet.getString(USER_ID));
            task.setProjectId(resultSet.getString(PROJECT_ID));
            task.setName(resultSet.getString(NAME));
            task.setDescription(resultSet.getString(DESCRIPTION));
            task.setDateOfCreation(resultSet.getDate(DATE_OF_CREATION));
            task.setDateStart(resultSet.getDate(DATE_START));
            task.setDateFinish(resultSet.getDate(DATE_FINISH));
            task.setStatus(Status.valueOf(resultSet.getString(STATUS)));
            tasks.add(task);
        }
        return tasks;
    }

}