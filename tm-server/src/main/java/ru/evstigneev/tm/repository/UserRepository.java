package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.IUserRepository;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private static final String ID = "id";
    @NotNull
    private static final String LOGIN = "login";
    @NotNull
    private static final String PASSWORD = "password";
    @NotNull
    private static final String ROLE = "role";

    public UserRepository(Connection connection) {
        super(connection);
    }

    @Override
    public boolean create(@NotNull final String login, @NotNull final String password) throws SQLException {
        @NotNull final String query = "insert into users values(?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, UUID.randomUUID().toString());
        statement.setString(2, login);
        statement.setString(3, password);
        statement.setString(4, RoleType.ADMIN.displayName());
        return statement.execute();
    }

    @Override
    public boolean create(@NotNull final String login, @NotNull final String password, @NotNull final RoleType role) throws SQLException {
        @NotNull final String query = "insert into users values(?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, UUID.randomUUID().toString());
        statement.setString(2, login);
        statement.setString(3, password);
        statement.setString(4, role.displayName());
        return statement.execute();
    }

    @Override
    public boolean updatePassword(@NotNull final String userId, @NotNull final String newPassword) throws SQLException {
        @NotNull final String query = "update users set password = ?  where id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, newPassword);
        statement.setString(2, userId);
        return statement.executeUpdate() > 0;
    }

    @Override
    public User findByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String query = "select id, login, password, role from users where login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        return fetch(statement.executeQuery());
    }

    @Override
    public Collection<User> findAll() throws SQLException {
        @NotNull final String query = "select id, login, password, role from users";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        return fetchAll(statement.executeQuery());
    }

    @Override
    public boolean update(@NotNull final String userId, @NotNull final String login, @NotNull final String password,
                          @NotNull final RoleType role) throws SQLException {
        @NotNull final String query = "update users set login = ?, password = ?, role = ? where id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        statement.setString(2, password);
        statement.setString(3, role.displayName());
        statement.setString(4, userId);
        return statement.executeUpdate() > 0;
    }

    @Override
    public boolean remove(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "delete from users where id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        return statement.execute();
    }

    @Override
    public boolean merge(@NotNull final String userId, @NotNull final User user) throws SQLException {
        @NotNull final User foundUser = findUserById(userId);
        if (foundUser != null) {
            return update(foundUser.getId(), foundUser.getLogin(), foundUser.getPasswordHash(), foundUser.getRole());
        }
        return persist(user);
    }

    private User findUserById(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "select id, login, password, role from users where id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        return fetch(statement.executeQuery());
    }

    @Override
    public boolean persist(@NotNull final User user) throws SQLException {
        @NotNull final String query = "insert into users values(?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getRole().displayName());
        return statement.execute();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "delete from users";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.execute();
    }

    @Nullable
    private User fetch(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        @NotNull final User user = new User();
        resultSet.next();
        user.setId(resultSet.getString(ID));
        user.setLogin(resultSet.getString(LOGIN));
        user.setPasswordHash(resultSet.getString(PASSWORD));
        user.setRole(RoleType.valueOf(resultSet.getString(ROLE)));
        return user;
    }

    @Nullable
    private Collection<User> fetchAll(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        @NotNull final Collection<User> users = new ArrayList<>();
        while (resultSet.next()) {
            @NotNull final User user = new User();
            user.setId(resultSet.getString(ID));
            user.setLogin(resultSet.getString(LOGIN));
            user.setPasswordHash(resultSet.getString(PASSWORD));
            user.setRole(RoleType.valueOf(resultSet.getString(ROLE)));
            users.add(user);
        }
        return users;
    }

}