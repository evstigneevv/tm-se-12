package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.IProjectRepository;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    private static final String PROJECT_ID = "id";
    @NotNull
    private static final String USER_ID = "user_id";
    @NotNull
    private static final String NAME = "name";
    @NotNull
    private static final String DESCRIPTION = "description";
    @NotNull
    private static final String DATE_OF_CREATION = "date_of_creation";
    @NotNull
    private static final String DATE_START = "date_start";
    @NotNull
    private static final String DATE_FINISH = "date_finish";
    @NotNull
    private static final String STATUS = "status";

    public ProjectRepository(Connection connection) {
        super(connection);
    }

    @Override
    public boolean create(@NotNull final String userId, @NotNull final String projectName,
                          @NotNull final String description) throws SQLException {
        @NotNull final String query = "insert into projects(id, user_id, name, description, date_of_creation, status)" +
                "values(?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, UUID.randomUUID().toString());
        statement.setString(2, userId);
        statement.setString(3, projectName);
        statement.setString(4, description);
        statement.setDate(5, new java.sql.Date(System.currentTimeMillis()));
        statement.setString(6, Status.PLANNING.displayName());
        return statement.execute();
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        @NotNull final String query = "delete from projects where id = ? and user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        return statement.execute();
    }

    @Override
    public boolean update(@NotNull final String userId, @NotNull final String projectId,
                          @NotNull final String name, @Nullable final String description,
                          @Nullable final Date dateStart, @Nullable final Date dateFinish, @NotNull final Status status) throws SQLException {
        @NotNull final String query = "update projects set name = ?, description = ?, date_start = ?, date_finish = " +
                "?, status = ? where id = ? and user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setDate(6, dateStart == null ? null : new java.sql.Date(dateStart.getTime()));
        statement.setDate(7, dateFinish == null ? null : new java.sql.Date(dateFinish.getTime()));
        statement.setString(5, status.displayName());
        statement.setString(6, projectId);
        statement.setString(7, userId);
        return statement.executeUpdate() > 0;
    }

    @Override
    public Collection<Project> findAll() throws SQLException {
        @NotNull final String query = "select id, user_id, name, description, date_of_creation, date_start, " +
                "date_finish, status from projects";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        return fetchAll(statement.executeQuery());
    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "select id, user_id, name, description, date_of_creation, date_start, date_finish," +
                " status from projects where user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        return fetchAll(statement.executeQuery());
    }

    @Override
    public Project findOne(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        @NotNull final String query = "select id, user_id, name, description, date_of_creation, date_start, date_finish," +
                " status from projects where id = ? and user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        return fetch(statement.executeQuery());
    }

    @Override
    public boolean merge(@NotNull final String userId, @NotNull final Project project) throws SQLException {
        @NotNull final Project foundProject = findOne(userId, project.getId());
        if (foundProject != null) {
            return update(foundProject.getUserId(), foundProject.getId(), foundProject.getName(),
                    foundProject.getDescription(), foundProject.getDateStart(), foundProject.getDateFinish(),
                    foundProject.getStatus());
        }
        return persist(project);
    }

    @Override
    public boolean persist(@NotNull final Project project) throws SQLException {
        @NotNull final String query = "insert into projects values(?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getUserId());
        statement.setString(3, project.getName());
        statement.setString(4, project.getDescription());
        statement.setDate(5, new java.sql.Date(project.getDateOfCreation().getTime()));
        statement.setDate(6, project.getDateStart() == null ? null : new java.sql.Date(project.getDateStart().getTime()));
        statement.setDate(7, project.getDateFinish() == null ? null : new java.sql.Date(project.getDateFinish().getTime()));
        statement.setString(8, project.getStatus().displayName());
        return statement.execute();
    }

    @Override
    public boolean removeAllByUserId(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "delete from projects where user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        return statement.execute();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "delete from projects";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.execute();
    }

    @Nullable
    private Project fetch(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        if (resultSet.next()) {
            @NotNull final Project project = new Project();
            project.setId(resultSet.getString(PROJECT_ID));
            project.setUserId(resultSet.getString(USER_ID));
            project.setName(resultSet.getString(NAME));
            project.setDescription(resultSet.getString(DESCRIPTION));
            project.setDateOfCreation(resultSet.getDate(DATE_OF_CREATION));
            project.setDateStart(resultSet.getDate(DATE_START));
            project.setDateFinish(resultSet.getDate(DATE_FINISH));
            project.setStatus(Status.valueOf(resultSet.getString(STATUS)));
            return project;
        }
        return null;
    }

    @Nullable
    private Collection<Project> fetchAll(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        @NotNull final Collection<Project> projects = new ArrayList<>();
        while (resultSet.next()) {
            @NotNull final Project project = new Project();
            project.setId(resultSet.getString(PROJECT_ID));
            project.setUserId(resultSet.getString(USER_ID));
            project.setName(resultSet.getString(NAME));
            project.setDescription(resultSet.getString(DESCRIPTION));
            project.setDateOfCreation(resultSet.getDate(DATE_OF_CREATION));
            project.setDateStart(resultSet.getDate(DATE_START));
            project.setDateFinish(resultSet.getDate(DATE_FINISH));
            project.setStatus(Status.valueOf(resultSet.getString(STATUS)));
            projects.add(project);
        }
        return projects;
    }

}
