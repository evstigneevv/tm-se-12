package ru.evstigneev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.endpoint.ITaskEndpoint;
import ru.evstigneev.tm.api.service.IProjectService;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.api.service.ITaskService;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.exception.RepositoryException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService(endpointInterface = "ru.evstigneev.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    @NotNull
    private ITaskService taskService;
    @NotNull
    private IProjectService projectService;
    @NotNull
    private ISessionService sessionService;

    public TaskEndpoint() {
    }

    public TaskEndpoint(@NotNull final ITaskService taskService, @NotNull final IProjectService projectService,
                        @NotNull final ISessionService sessionService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public boolean createTask(@WebParam(name = "session") @Nullable final Session session,
                              @WebParam(name = "projectId") @NotNull final String projectId,
                              @WebParam(name = "description") @NotNull final String taskName,
                              @WebParam(name = "description") @NotNull final String description) throws Exception {
        sessionService.validate(session);
        for (Project project : projectService.findAllByUserId(session.getUserId())) {
            if (project.getId().equals(projectId)) {
                return taskService.create(session.getUserId(), projectId, taskName, description);
            }
        }
        throw new RepositoryException();
    }

    @Override
    @WebMethod
    public Collection<Task> findAllTasks(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, RoleType.ADMIN);
        return taskService.findAll();
    }

    @Override
    @WebMethod
    public Collection<Task> findAllTasksByUserId(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session);
        return taskService.findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public boolean removeTask(@WebParam(name = "session") @Nullable final Session session,
                              @WebParam(name = "taskId") @NotNull final String taskId) throws Exception {
        sessionService.validate(session);
        return taskService.remove(session.getUserId(), taskId);
    }

    @Override
    @WebMethod
    public Collection<Task> getTaskListByProjectId(@WebParam(name = "session") @Nullable final Session session,
                                                   @WebParam(name = "projectId") @NotNull final String projectId) throws Exception {
        sessionService.validate(session);
        return taskService.getTaskListByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public boolean updateTask(@WebParam(name = "session") @Nullable final Session session,
                              @WebParam(name = "taskId") @NotNull final String taskId,
                              @WebParam(name = "taskName") @NotNull final String taskName,
                              @WebParam(name = "description") @Nullable final String description,
                              @WebParam(name = "dateStart") @Nullable final String dateStart,
                              @WebParam(name = "dateFinish") @Nullable final String dateFinish,
                              @WebParam(name = "status") @NotNull final Status status) throws Exception {
        sessionService.validate(session);
        return taskService.update(session.getUserId(), taskId, taskName, description, dateStart, dateFinish, status);
    }

    @Override
    @WebMethod
    public boolean mergeTask(@WebParam(name = "session") @Nullable final Session session,
                             @WebParam(name = "task") @NotNull final Task task) throws Exception {
        sessionService.validate(session);
        return taskService.merge(session.getUserId(), task);
    }

    @Override
    @WebMethod
    public boolean persistTask(@WebParam(name = "session") @Nullable final Session session,
                               @WebParam(name = "task") @NotNull final Task task) throws Exception {
        sessionService.validate(session);
        return taskService.persist(session.getUserId(), task);
    }

    @Override
    @WebMethod
    public void removeAllTasks(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, RoleType.ADMIN);
        taskService.removeAll();
    }

    @Override
    @WebMethod
    public boolean removeAllTasksByUserId(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session);
        return taskService.removeAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public boolean deleteAllProjectTasks(@WebParam(name = "session") @Nullable final Session session,
                                         @WebParam(name = "projectId") @NotNull final String projectId) throws Exception {
        sessionService.validate(session);
        return taskService.deleteAllProjectTasks(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public Task findOne(@WebParam(name = "session") @Nullable final Session session,
                        @WebParam(name = "projectId") @NotNull final String projectId,
                        @WebParam(name = "taskId") @NotNull final String taskId) throws Exception {
        sessionService.validate(session);
        return taskService.findOne(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    public List<Task> sortTasks(@WebParam(name = "session") @Nullable final Session session,
                                @WebParam(name = "comparatorName") @NotNull final String comparatorName) throws Exception {
        sessionService.validate(session);
        return taskService.sort(comparatorName);
    }

    @Override
    @WebMethod
    public List<Task> searchTaskByString(@WebParam(name = "session") @Nullable final Session session,
                                         @WebParam(name = "string") @NotNull final String string) throws Exception {
        sessionService.validate(session);
        return taskService.searchByString(string);
    }

}