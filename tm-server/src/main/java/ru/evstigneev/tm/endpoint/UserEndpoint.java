package ru.evstigneev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.endpoint.IUserEndpoint;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.api.service.IUserService;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService(endpointInterface = "ru.evstigneev.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    @NotNull
    private IUserService userService;
    @NotNull
    private ISessionService sessionService;

    public UserEndpoint() {
    }

    public UserEndpoint(@NotNull final IUserService userService, @NotNull final ISessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public boolean createAdmin(@WebParam(name = "login") @NotNull final String login,
                               @WebParam(name = "password") @NotNull final String password) throws Exception {
        return userService.create(login, password);
    }

    @Override
    @WebMethod
    public boolean createUser(@WebParam(name = "session") @Nullable final Session session,
                              @WebParam(name = "login") @NotNull final String login,
                              @WebParam(name = "password") @NotNull final String password,
                              @WebParam(name = "role") @NotNull final RoleType role) throws Exception {
        sessionService.validate(session, RoleType.ADMIN);
        return userService.create(login, password, role);
    }

    @Override
    @WebMethod
    public Collection<User> findAllUsers(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, RoleType.ADMIN);
        return userService.findAll();
    }

    @Override
    @WebMethod
    public boolean checkPassword(@WebParam(name = "login") @NotNull final String login,
                                 @WebParam(name = "password") @NotNull final String password) throws Exception {
        return userService.checkPassword(login, password);
    }

    @Override
    @WebMethod
    public boolean checkPasswordByUserId(@WebParam(name = "userId") @NotNull final String userId,
                                         @WebParam(name = "password") @NotNull final String password) throws Exception {
        return userService.checkPasswordByUserId(userId, password);
    }

    @Override
    @WebMethod
    public boolean updatePassword(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "newPassword") @NotNull final String newPassword) throws Exception {
        sessionService.validate(session);
        return userService.updatePassword(session.getUserId(), newPassword);
    }

    @Override
    @WebMethod
    public User findUserByLogin(@WebParam(name = "session") @Nullable final Session session,
                                @WebParam(name = "login") @NotNull final String login) throws Exception {
        sessionService.validate(session, RoleType.ADMIN);
        return userService.findByLogin(login);
    }

    @Override
    @WebMethod
    public void updateUser(@WebParam(name = "session") @Nullable final Session session,
                           @WebParam(name = "login") @NotNull final String login,
                           @WebParam(name = "password") @NotNull final String password,
                           @WebParam(name = "role") @NotNull final RoleType role) throws Exception {
        sessionService.validate(session, RoleType.ADMIN);
        userService.update(session.getUserId(), login, password, role);
    }

    @Override
    @WebMethod
    public boolean removeUser(@WebParam(name = "session") @Nullable final Session session,
                              @WebParam(name = "userId") @NotNull final String userId) throws Exception {
        sessionService.validate(session, RoleType.ADMIN);
        return userService.remove(userId);
    }

    @Override
    @WebMethod
    public boolean persistUser(@WebParam(name = "session") @Nullable final Session session,
                               @WebParam(name = "user") @NotNull final User user) throws Exception {
        sessionService.validate(session);
        return userService.persist(session.getUserId(), user);
    }

    @Override
    @WebMethod
    public void removeAllUsers(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, RoleType.ADMIN);
        userService.removeAll();
    }

    @Override
    @WebMethod
    public boolean mergeUser(@WebParam(name = "session") @Nullable final Session session,
                             @WebParam(name = "user") @NotNull final User user) throws Exception {
        sessionService.validate(session, RoleType.ADMIN);
        return userService.merge(session.getUserId(), user);
    }

    @WebMethod
    public boolean isEmptyUserList() throws Exception {
        return userService.findAll().isEmpty();
    }

}
