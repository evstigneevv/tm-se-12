package ru.evstigneev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.enumerated.RoleType;

@Getter
@Setter
@ToString
public class Session implements Cloneable {

    @NotNull
    private String id;
    @NotNull
    private String UserId;
    @NotNull
    private RoleType roleType;
    @NotNull
    private Long timestamp;
    @Nullable
    transient private String signature;

    @Override
    public Session clone() throws CloneNotSupportedException {
        return (Session) super.clone();
    }

}
