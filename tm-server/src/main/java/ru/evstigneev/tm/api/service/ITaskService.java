package ru.evstigneev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    boolean create(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name,
                   @NotNull final String description) throws Exception;

    Collection<Task> findAll() throws Exception;

    Collection<Task> findAllByUserId(@NotNull final String userId) throws Exception;

    boolean remove(@NotNull final String userId, @NotNull final String taskId) throws Exception;

    Collection<Task> getTaskListByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception;

    boolean update(@NotNull final String userId, @NotNull final String taskId,
                   @NotNull final String name, @Nullable final String description,
                   @Nullable final String dateStart, @Nullable final String dateFinish,
                   @NotNull final Status status) throws Exception;

    Task findOne(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws Exception;

    boolean merge(@NotNull final String userId, @NotNull final Task task) throws Exception;

    boolean persist(@NotNull final String userId, @NotNull final Task task) throws Exception;

    void removeAll() throws Exception;

    boolean removeAllByUserId(@NotNull final String userId) throws Exception;

    boolean deleteAllProjectTasks(@NotNull final String userId, @NotNull final String projectId) throws Exception;

    List<Task> sort(@NotNull final String comparator) throws Exception;

    List<Task> searchByString(@NotNull final String string) throws Exception;

}
