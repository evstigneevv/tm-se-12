package ru.evstigneev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.sql.SQLException;

public interface ISessionRepository {

    boolean persist(@NotNull final Session session) throws EmptyStringException, SQLException;

    boolean removeByUserId(@NotNull final String userId) throws SQLException;

}
