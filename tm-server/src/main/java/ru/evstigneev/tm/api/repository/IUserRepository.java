package ru.evstigneev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.sql.SQLException;
import java.util.Collection;

public interface IUserRepository {

    boolean create(@NotNull final String login, @NotNull final String password) throws SQLException;

    boolean create(@NotNull final String login, @NotNull final String password, @NotNull final RoleType role) throws SQLException;

    boolean updatePassword(@NotNull final String userId, @NotNull final String newPassword) throws SQLException;

    User findByLogin(@NotNull final String login) throws EmptyStringException, SQLException;

    Collection<User> findAll() throws SQLException;

    boolean update(@NotNull final String userId, @NotNull final String login, @NotNull final String password,
                   @NotNull final RoleType role) throws SQLException;

    boolean remove(@NotNull final String userId) throws SQLException;

    boolean persist(@NotNull final User user) throws SQLException;

    boolean merge(@NotNull final String userId, @NotNull final User user) throws SQLException;

    void removeAll() throws SQLException;

}
