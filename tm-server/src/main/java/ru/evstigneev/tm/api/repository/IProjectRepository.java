package ru.evstigneev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.enumerated.Status;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

public interface IProjectRepository {

    boolean create(@NotNull final String userId, @NotNull final String projectName,
                   @NotNull final String description) throws SQLException;

    boolean remove(@NotNull final String userId, @NotNull final String projectId) throws SQLException;

    boolean update(@NotNull final String userId, @NotNull final String projectId,
                   @NotNull final String name, @Nullable final String description,
                   @Nullable final Date dateStart, @Nullable final Date dateFinish, @NotNull final Status status) throws SQLException;

    Collection<Project> findAll() throws Exception;

    Collection<Project> findAllByUserId(@NotNull final String userId) throws SQLException;

    Project findOne(@NotNull final String userId, @NotNull final String projectId) throws SQLException;

    boolean merge(@NotNull final String userId, @NotNull final Project project) throws SQLException;

    boolean persist(@NotNull final Project project) throws SQLException;

    boolean removeAllByUserId(@NotNull final String userId) throws SQLException;

    void removeAll() throws SQLException;

}
