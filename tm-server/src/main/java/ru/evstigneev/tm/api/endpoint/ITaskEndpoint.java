package ru.evstigneev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    String URL = "http://localhost:8080/TaskEndpoint?wsdl";

    @WebMethod
    boolean createTask(@WebParam(name = "session") @Nullable final Session session,
                       @WebParam(name = "projectId") @NotNull final String projectId,
                       @WebParam(name = "taskName") @NotNull final String taskName,
                       @WebParam(name = "description") @NotNull final String description) throws Exception;

    @WebMethod
    Collection<Task> findAllTasks(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    Collection<Task> findAllTasksByUserId(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    boolean removeTask(@WebParam(name = "session") @NotNull final Session session,
                       @WebParam(name = "taskId") @NotNull final String taskId) throws Exception;

    @WebMethod
    Collection<Task> getTaskListByProjectId(@WebParam(name = "session") @NotNull final Session session,
                                            @WebParam(name = "projectId") @NotNull final String projectId) throws Exception;

    @WebMethod
    boolean updateTask(@WebParam(name = "session") @Nullable final Session session,
                       @WebParam(name = "taskId") @NotNull final String taskId,
                       @WebParam(name = "taskName") @NotNull final String taskName,
                       @WebParam(name = "description") @Nullable final String description,
                       @WebParam(name = "dateStart") @Nullable final String dateStart,
                       @WebParam(name = "dateFinish") @Nullable final String dateFinish,
                       @WebParam(name = "status") @NotNull final Status status) throws Exception;

    @WebMethod
    boolean mergeTask(@WebParam(name = "session") @NotNull final Session session,
                      @WebParam(name = "task") @NotNull final Task task) throws Exception;

    @WebMethod
    boolean persistTask(@WebParam(name = "session") @NotNull final Session session,
                        @WebParam(name = "task") @NotNull final Task task) throws Exception;

    @WebMethod
    void removeAllTasks(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    boolean removeAllTasksByUserId(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    boolean deleteAllProjectTasks(@WebParam(name = "session") @NotNull final Session session,
                                  @WebParam(name = "projectId") @NotNull final String projectId) throws Exception;

    @WebMethod
    Task findOne(@WebParam(name = "session") @NotNull final Session session,
                 @WebParam(name = "projectId") @NotNull final String projectId,
                 @WebParam(name = "taskId") @NotNull final String taskId) throws Exception;

    @WebMethod
    List<Task> sortTasks(@WebParam(name = "session") @NotNull final Session session,
                         @WebParam(name = "comparatorName") @NotNull final String comparatorName) throws Exception;

    @WebMethod
    List<Task> searchTaskByString(@WebParam(name = "session") @NotNull final Session session,
                                  @WebParam(name = "string") @NotNull final String string) throws Exception;

}
