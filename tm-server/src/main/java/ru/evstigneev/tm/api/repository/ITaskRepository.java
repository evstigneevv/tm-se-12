package ru.evstigneev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

public interface ITaskRepository {

    boolean create(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name,
                   @NotNull final String description) throws SQLException;

    boolean remove(@NotNull final String userId, @NotNull final String taskId) throws EmptyStringException, SQLException;

    boolean update(@NotNull final String userId, @NotNull final String taskId,
                   @NotNull final String name, @NotNull final String description,
                   @NotNull final Date dateStart, @NotNull final Date dateFinish, @NotNull final Status status) throws SQLException;

    Collection<Task> getTaskListByProjectId(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException, SQLException;

    Collection<Task> findAll() throws SQLException;

    Collection<Task> findAllByUserId(@NotNull final String userId) throws EmptyStringException, SQLException;

    boolean deleteAllProjectTasks(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException, SQLException;

    Task findOne(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws EmptyStringException, SQLException;

    void removeAll() throws SQLException;

    boolean removeAllByUserId(@NotNull final String userId) throws EmptyStringException, SQLException;

    boolean merge(@NotNull final String userId, @NotNull final Task task) throws Exception;

    boolean persist(@NotNull final Task task) throws Exception;

}
