package ru.evstigneev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.sql.SQLException;
import java.util.Collection;

public interface IUserService {

    boolean create(@NotNull final String login, @NotNull final String password) throws Exception;

    boolean create(@NotNull final String login, @NotNull final String password, @NotNull final RoleType role) throws Exception;

    Collection<User> findAll() throws Exception;

    boolean checkPassword(@NotNull final String login, @NotNull final String password) throws Exception;

    boolean checkPasswordByUserId(@NotNull final String userId, @NotNull final String password) throws Exception;

    boolean updatePassword(@NotNull final String userId, @NotNull final String newPassword) throws Exception;

    User findByLogin(@NotNull final String login) throws Exception;

    void update(@NotNull final String userId, @NotNull final String login, @NotNull final String password,
                @NotNull final RoleType role) throws Exception;

    boolean remove(@NotNull final String userId) throws Exception;

    boolean persist(@NotNull final String userId, @NotNull final User user) throws Exception;

    void removeAll() throws Exception;

    boolean merge(@NotNull final String userId, @NotNull final User user) throws Exception;

}
