package ru.evstigneev.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Project;

import java.util.Comparator;

public class ComparatorProjectDateFinish implements Comparator<Project> {

    @Override
    public int compare(@NotNull final Project o1, @NotNull final Project o2) {
        if (o1.getDateFinish() != null && o2.getDateFinish() == null) {
            return -1;
        }
        if (o1.getDateFinish() == null && o2.getDateFinish() != null) {
            return 1;
        }
        if (o1.getDateFinish() == null || o2.getDateFinish() == null) {
            return 0;
        }
        return o1.getDateFinish().compareTo(o2.getDateFinish());
    }

}
