package ru.evstigneev.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Project;

import java.util.Comparator;

public class ComparatorProjectDateCreation implements Comparator<Project> {

    @Override
    public int compare(@NotNull final Project o1, @NotNull final Project o2) {
        return o1.getDateOfCreation().compareTo(o2.getDateOfCreation());
    }

}
