package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.ITaskRepository;
import ru.evstigneev.tm.api.service.ITaskService;
import ru.evstigneev.tm.comparator.ComparatorTaskDateCreation;
import ru.evstigneev.tm.comparator.ComparatorTaskDateFinish;
import ru.evstigneev.tm.comparator.ComparatorTaskDateStart;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.exception.CommandCorruptException;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.repository.TaskRepository;
import ru.evstigneev.tm.util.DBConnector;
import ru.evstigneev.tm.util.DateParser;

import java.util.*;

public class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private static final String DATE_START_COMPARATOR_NAME = "date start";
    @NotNull
    private static final String DATE_FINISH_COMPARATOR_NAME = "date finish";
    @NotNull
    private static final String DATE_CREATION_COMPARATOR_NAME = "date creation";
    @NotNull
    private static final String STATUS_COMPARATOR_NAME = "status";

    @Override
    public boolean create(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name,
                          @NotNull final String description) throws Exception {
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        if (userId.isEmpty() || projectId.isEmpty() || name.isEmpty()) throw new EmptyStringException();
        return taskRepository.create(userId, projectId, name, description);
    }

    @Override
    public Collection<Task> findAll() throws Exception {
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        return taskRepository.findAll();
    }

    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) throws Exception {
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        if (userId.isEmpty()) throw new EmptyStringException();
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String taskId) throws Exception {
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        if (userId.isEmpty() || taskId.isEmpty()) throw new EmptyStringException();
        return taskRepository.remove(userId, taskId);
    }

    @Override
    public Collection<Task> getTaskListByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyStringException();
        return taskRepository.getTaskListByProjectId(userId, projectId);
    }

    @Override
    public boolean update(@NotNull final String userId, @NotNull final String taskId,
                          @NotNull final String name, @NotNull final String description,
                          @NotNull final String dateStart, @NotNull final String dateFinish,
                          @NotNull final Status status) throws Exception {
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        if (userId.isEmpty() || taskId.isEmpty() || name.isEmpty()) throw new EmptyStringException();
        return taskRepository.update(userId, taskId, name, description, DateParser.setDateByString(dateStart),
                DateParser.setDateByString(dateFinish), status);
    }

    @Override
    public boolean merge(@NotNull final String userId, @NotNull final Task task) throws Exception {
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        if (userId.isEmpty()) throw new EmptyStringException();
        return taskRepository.merge(userId, task);
    }

    @Override
    public boolean persist(@NotNull final String userId, @NotNull final Task task) throws Exception {
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        if (userId.isEmpty()) throw new EmptyStringException();
        return taskRepository.persist(task);
    }

    @Override
    public void removeAll() throws Exception {
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        taskRepository.removeAll();
    }

    @Override
    public boolean removeAllByUserId(@NotNull final String userId) throws Exception {
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        if (userId.isEmpty()) throw new EmptyStringException();
        return taskRepository.removeAllByUserId(userId);
    }

    @Override
    public boolean deleteAllProjectTasks(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyStringException();
        return taskRepository.deleteAllProjectTasks(userId, projectId);
    }

    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws Exception {
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        if (userId.isEmpty() || projectId.isEmpty() || taskId.isEmpty()) throw new EmptyStringException();
        return taskRepository.findOne(userId, projectId, taskId);
    }

    public List<Task> sort(@NotNull final String comparatorName) throws Exception {
        @NotNull final List<Task> sortedList = new ArrayList<>(findAll());
        @Nullable final Comparator<Task> comparator = findComparator(comparatorName);
        if (comparator == null) {
            Collections.sort(sortedList);
            return sortedList;
        }
        Collections.sort(sortedList, comparator);
        return sortedList;
    }

    private Comparator<Task> findComparator(@NotNull final String name) throws CommandCorruptException {
        switch (name.toLowerCase()) {
            default:
                throw new CommandCorruptException();
            case DATE_START_COMPARATOR_NAME: {
                return new ComparatorTaskDateStart();
            }
            case DATE_FINISH_COMPARATOR_NAME: {
                return new ComparatorTaskDateFinish();
            }
            case DATE_CREATION_COMPARATOR_NAME: {
                return new ComparatorTaskDateCreation();
            }
            case STATUS_COMPARATOR_NAME: {
                return null;
            }
        }
    }

    public List<Task> searchByString(@NotNull final String string) throws Exception {
        if (string.isEmpty()) throw new EmptyStringException();
        @Nullable final List<Task> projectList = new ArrayList<>();
        for (Task task : findAll()) {
            if (task.getName().contains(string) || (task.getDescription() != null && task.getDescription().contains(string))) {
                projectList.add(task);
            }
        }
        return projectList;
    }

}
