package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.IProjectRepository;
import ru.evstigneev.tm.api.repository.IUserRepository;
import ru.evstigneev.tm.api.service.IUserService;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.repository.ProjectRepository;
import ru.evstigneev.tm.repository.UserRepository;
import ru.evstigneev.tm.util.DBConnector;
import ru.evstigneev.tm.util.PasswordParser;

import java.util.Collection;

public class UserService extends AbstractService<User> implements IUserService {

    @Override
    public boolean create(@NotNull final String login, @NotNull final String password) throws Exception {
        @Nullable final IUserRepository userRepository = new UserRepository(DBConnector.getConnection());
        if (login.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        return userRepository.create(login, PasswordParser.getPasswordHash(password));
    }

    @Override
    public boolean create(@NotNull final String login, @NotNull final String password, @NotNull final RoleType role) throws Exception {
        @Nullable final IUserRepository userRepository = new UserRepository(DBConnector.getConnection());
        if (login.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        return userRepository.create(login, PasswordParser.getPasswordHash(password), role);
    }

    @Override
    public Collection<User> findAll() throws Exception {
        @Nullable final IUserRepository userRepository = new UserRepository(DBConnector.getConnection());
        return userRepository.findAll();
    }

    @Override
    public boolean checkPassword(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        @Nullable final Collection<User> users = findAll();
        if (users != null) {
            for (User user : users) {
                if (user.getLogin().equals(login)) {
                    return PasswordParser.getPasswordHash(password).equals(user.getPasswordHash());
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkPasswordByUserId(@NotNull final String userId, @NotNull final String password) throws Exception {
        if (userId.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        @Nullable final Collection<User> users = findAll();
        if (users != null) {
            for (User user : users) {
                if (user.getId().equals(userId)) {
                    return PasswordParser.getPasswordHash(password).equals(user.getPasswordHash());
                }
            }
        }
        return false;
    }

    @Override
    public boolean updatePassword(@NotNull final String userId, @NotNull final String newPassword) throws Exception {
        @Nullable final IUserRepository userRepository = new UserRepository(DBConnector.getConnection());
        if (userId.isEmpty() || newPassword.isEmpty()) throw new EmptyStringException();
        return userRepository.updatePassword(userId, PasswordParser.getPasswordHash(newPassword));
    }

    @Override
    public User findByLogin(@NotNull final String login) throws Exception {
        @Nullable final UserRepository userRepository = new UserRepository(DBConnector.getConnection());
        if (login.isEmpty()) throw new EmptyStringException();
        return userRepository.findByLogin(login);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String login, @NotNull final String password,
                       @NotNull final RoleType role) throws Exception {
        @Nullable final IUserRepository userRepository = new UserRepository(DBConnector.getConnection());
        if (userId.isEmpty() || login.isEmpty()) throw new EmptyStringException();
        userRepository.update(userId, login, PasswordParser.getPasswordHash(password), role);
    }

    @Override
    public boolean remove(@NotNull final String userId) throws Exception {
        @Nullable final IUserRepository userRepository = new UserRepository(DBConnector.getConnection());
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(DBConnector.getConnection());
        if (userId.isEmpty()) throw new EmptyStringException();
        projectRepository.removeAllByUserId(userId);
        return userRepository.remove(userId);
    }

    @Override
    public boolean persist(@NotNull final String userId, @NotNull final User user) throws Exception {
        @Nullable final IUserRepository userRepository = new UserRepository(DBConnector.getConnection());
        if (userId.isEmpty()) throw new EmptyStringException();
        return userRepository.persist(user);
    }

    @Override
    public void removeAll() throws Exception {
        @Nullable final IUserRepository userRepository = new UserRepository(DBConnector.getConnection());
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(DBConnector.getConnection());
        projectRepository.removeAll();
        userRepository.removeAll();
    }

    @Override
    public boolean merge(@NotNull final String userId, @NotNull final User user) throws Exception {
        @Nullable final IUserRepository userRepository = new UserRepository(DBConnector.getConnection());
        if (userId.isEmpty()) throw new EmptyStringException();
        return userRepository.merge(userId, user);
    }

}
