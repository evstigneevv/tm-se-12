package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.ISessionRepository;
import ru.evstigneev.tm.api.repository.IUserRepository;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.NoPermissionException;
import ru.evstigneev.tm.repository.SessionRepository;
import ru.evstigneev.tm.repository.UserRepository;
import ru.evstigneev.tm.util.DBConnector;
import ru.evstigneev.tm.util.SignatureCalculator;

import java.util.UUID;

public class SessionService implements ISessionService {

    private static final long SESSION_EXPIRATION_TIME = 1_800_000L;

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) throw new NoPermissionException();
        if (session.getSignature() == null || session.getId().isEmpty() || session.getUserId().isEmpty()
                || session.getRoleType().displayName().isEmpty() || session.getSignature().isEmpty()) {
            throw new NoPermissionException();
        }
        if (System.currentTimeMillis() - session.getTimestamp() > SESSION_EXPIRATION_TIME) {
            throw new NoPermissionException();
        }
        @NotNull final Session temp = session.clone();
        temp.setSignature(null);
        temp.setSignature(SignatureCalculator.calculateSignature(temp));
        if (temp.getSignature() != null) {
            if (!temp.getSignature().equals(session.getSignature())) {
                throw new NoPermissionException();
            }
        }
    }

    @Override
    public void validate(@Nullable final Session session, @NotNull final RoleType roleType) throws Exception {
        validate(session);
        if (!session.getRoleType().equals(roleType)) throw new NoPermissionException();
    }

    @Override
    public Session openSession(@NotNull final String login, @NotNull final String password) throws Exception {
        @Nullable final ISessionRepository sessionRepository = new SessionRepository(DBConnector.getConnection());
        @Nullable final IUserRepository userRepository = new UserRepository(DBConnector.getConnection());
        if (login.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        @NotNull final Session session = new Session();
        @NotNull final User user = userRepository.findByLogin(login);
        session.setUserId(user.getId());
        session.setId(UUID.randomUUID().toString());
        session.setRoleType(user.getRole());
        session.setTimestamp(System.currentTimeMillis());
        session.setSignature(SignatureCalculator.calculateSignature(session));
        sessionRepository.persist(session);
        return session;
    }

    @Override
    public boolean closeSession(@NotNull final Session session) throws Exception {
        @Nullable final ISessionRepository sessionRepository = new SessionRepository(DBConnector.getConnection());
        return sessionRepository.removeByUserId(session.getUserId());
    }

    @Override
    public boolean persist(@NotNull final String userId, @NotNull final Session session) throws Exception {
        @Nullable final ISessionRepository sessionRepository = new SessionRepository(DBConnector.getConnection());
        return sessionRepository.persist(session);
    }

    @Override
    public boolean removeByUserId(@NotNull final String userId) throws Exception {
        @Nullable final ISessionRepository sessionRepository = new SessionRepository(DBConnector.getConnection());
        return sessionRepository.removeByUserId(userId);
    }

}