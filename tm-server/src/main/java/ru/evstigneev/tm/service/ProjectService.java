package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.IProjectRepository;
import ru.evstigneev.tm.api.repository.ITaskRepository;
import ru.evstigneev.tm.api.service.IProjectService;
import ru.evstigneev.tm.comparator.ComparatorProjectDateCreation;
import ru.evstigneev.tm.comparator.ComparatorProjectDateFinish;
import ru.evstigneev.tm.comparator.ComparatorProjectDateStart;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.exception.CommandCorruptException;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.repository.ProjectRepository;
import ru.evstigneev.tm.repository.TaskRepository;
import ru.evstigneev.tm.util.DBConnector;
import ru.evstigneev.tm.util.DateParser;

import java.util.*;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private static final String DATE_START_COMPARATOR_NAME = "date start";
    @NotNull
    private static final String DATE_FINISH_COMPARATOR_NAME = "date finish";
    @NotNull
    private static final String DATE_CREATION_COMPARATOR_NAME = "date creation";
    @NotNull
    private static final String STATUS_COMPARATOR_NAME = "status";

    @Override
    public Collection<Project> findAll() throws Exception {
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(DBConnector.getConnection());
        return projectRepository.findAll();
    }

    @Override
    public boolean create(@NotNull final String userId, @NotNull final String projectName,
                          @NotNull final String description) throws Exception {
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(DBConnector.getConnection());
        if (userId.isEmpty() || projectName.isEmpty()) throw new EmptyStringException();
        return projectRepository.create(userId, projectName, description);
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(DBConnector.getConnection());
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyStringException();
        taskRepository.deleteAllProjectTasks(userId, projectId);
        return projectRepository.remove(userId, projectId);
    }

    @Override
    public boolean update(@NotNull final String userId, @NotNull final String projectId,
                          @NotNull final String name, @Nullable final String description,
                          @Nullable final String dateStart, @Nullable final String dateFinish,
                          @NotNull final Status status) throws Exception {
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(DBConnector.getConnection());
        if (userId.isEmpty() || projectId.isEmpty() || name.isEmpty()) throw new EmptyStringException();
        return projectRepository.update(userId, projectId, name, description, DateParser.setDateByString(dateStart),
                DateParser.setDateByString(dateFinish), status);
    }

    @Override
    public Project findOne(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(DBConnector.getConnection());
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyStringException();
        return projectRepository.findOne(userId, projectId);
    }

    @Override
    public boolean merge(@NotNull final String userId, @NotNull final Project project) throws Exception {
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(DBConnector.getConnection());
        if (userId.isEmpty()) throw new EmptyStringException();
        return projectRepository.merge(userId, project);
    }

    @Override
    public boolean persist(@NotNull final String userId, @NotNull final Project project) throws Exception {
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(DBConnector.getConnection());
        if (userId.isEmpty()) throw new EmptyStringException();
        return projectRepository.persist(project);
    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws Exception {
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(DBConnector.getConnection());
        if (userId.isEmpty()) throw new EmptyStringException();
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void removeAll() throws Exception {
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(DBConnector.getConnection());
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        taskRepository.removeAll();
        projectRepository.removeAll();
    }

    @Override
    public boolean removeAllByUserId(@NotNull final String userId) throws Exception {
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(DBConnector.getConnection());
        @Nullable final ITaskRepository taskRepository = new TaskRepository(DBConnector.getConnection());
        if (userId.isEmpty()) throw new EmptyStringException();
        taskRepository.removeAllByUserId(userId);
        return projectRepository.removeAllByUserId(userId);
    }

    public List<Project> sort(@NotNull final String comparatorName) throws Exception {
        @NotNull final List<Project> sortedList = new ArrayList<>(findAll());
        @Nullable final Comparator<Project> comparator = findComparator(comparatorName);
        if (comparator == null) {
            Collections.sort(sortedList);
            return sortedList;
        }
        Collections.sort(sortedList, comparator);
        return sortedList;
    }

    private Comparator<Project> findComparator(@NotNull final String name) throws CommandCorruptException {
        switch (name.toLowerCase()) {
            default:
                throw new CommandCorruptException();
            case DATE_START_COMPARATOR_NAME: {
                return new ComparatorProjectDateStart();
            }
            case DATE_FINISH_COMPARATOR_NAME: {
                return new ComparatorProjectDateFinish();
            }
            case DATE_CREATION_COMPARATOR_NAME: {
                return new ComparatorProjectDateCreation();
            }
            case STATUS_COMPARATOR_NAME: {
                return null;
            }
        }
    }

    public List<Project> searchByString(@NotNull final String string) throws Exception {
        if (string.isEmpty()) throw new EmptyStringException();
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (Project project : findAll()) {
            if (project.getName().contains(string) || (project.getDescription() != null && project.getDescription().contains(string))) {
                projectList.add(project);
            }
        }
        return projectList;
    }

}
