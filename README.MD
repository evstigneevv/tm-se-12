# Task Manager se-12 version 1.0.0
### Software requirements:
java 1.7, maven 3.3.9
***
### Technology stack:
jdk 1.8, maven 3
***
### developer:
Vladimir Evstigneev
email: godlique@gmail.com
***
### Maven commands:
mvn clean install
***
### Execution commands:
java -jar pathToTheJarFile\tm-se-12.jar