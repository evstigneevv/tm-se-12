package ru.evstigneev.tm.command.data;

import ru.evstigneev.tm.command.AbstractCommand;

public class DataWriteJacksonJsonCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA WRITE JACKSON JSON";
    }

    @Override
    public String description() {
        return "Write data to .json file with Jackson";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getDomainEndpoint().writeDataJacksonJson(bootstrap.getSession());
    }

}