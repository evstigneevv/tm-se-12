package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "CT";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("CREATE NEW TASK");
        System.out.println("enter project ID: ");
        @NotNull final String projectId = bootstrap.getScanner().nextLine();
        System.out.println("enter new task name into project: ");
        @NotNull final String taskName = bootstrap.getScanner().nextLine();
        System.out.println("enter description of task: ");
        bootstrap.getTaskEndpoint().createTask(bootstrap.getSession(), projectId, taskName,
                bootstrap.getScanner().nextLine());
        System.out.println("task created!");
    }

}
