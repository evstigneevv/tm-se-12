package ru.evstigneev.tm.command.data;

import ru.evstigneev.tm.command.AbstractCommand;

public class DataWriteJaxbJsonCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA WRITE JAXB JSON";
    }

    @Override
    public String description() {
        return "Save current data to .json file";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getDomainEndpoint().writeDataJaxbJson(bootstrap.getSession());
    }

}
