package ru.evstigneev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.Project;
import ru.evstigneev.tm.command.AbstractCommand;

public class ProjectGetOneByUserIdCommand extends AbstractCommand {

    @Override
    public String command() {
        return "PGAU";
    }

    @Override
    public String description() {
        return "Show all projects by user ID";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SHOW ALL PROJECTS");
        System.out.println("Enter project name: ");
        @NotNull final String projectId = bootstrap.getScanner().nextLine();
        @NotNull final Project project = bootstrap.getProjectEndpoint().findOneProject(bootstrap.getSession(), projectId);
        System.out.println("User ID: " + project.getUserId() + " | Project ID: " + project.getId() + " | Project name: "
                + project.getName() + " | Project description: " + project.getDescription() + " | Date of creation: "
                + project.getDateOfCreation() + " | Date of start: " + project.getDateStart() + " | Date of finish: "
                + project.getDateFinish() + " | Project status: " + project.getStatus());
        System.out.println();
    }

}
