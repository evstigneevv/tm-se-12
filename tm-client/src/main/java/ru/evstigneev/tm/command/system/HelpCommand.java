package ru.evstigneev.tm.command.system;

import ru.evstigneev.tm.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {

    @Override
    public String command() {
        return "HELP";
    }

    @Override
    public String description() {
        return "Show available commands and their description";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("HELP");
        for (AbstractCommand command : bootstrap.getCommands()) {
            System.out.println(command.command() + " " + command.description());
        }
        System.out.println();
    }

}
