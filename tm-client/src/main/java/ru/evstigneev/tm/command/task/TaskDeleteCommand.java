package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;

public class TaskDeleteCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DT";
    }

    @Override
    public String description() {
        return "Delete task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter user ID: ");
        @NotNull final String userId = bootstrap.getScanner().nextLine();
        if (bootstrap.getTaskEndpoint().removeAllTasksByUserId(bootstrap.getSession())) {
            System.out.println("Tasks was deleted!");
        } else {
            System.out.println("Tasks wasn't deleted!");
        }
    }

}
