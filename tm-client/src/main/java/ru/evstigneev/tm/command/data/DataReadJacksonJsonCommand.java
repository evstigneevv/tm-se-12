package ru.evstigneev.tm.command.data;

import ru.evstigneev.tm.command.AbstractCommand;

public class DataReadJacksonJsonCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA READ JACKSON JSON";
    }

    @Override
    public String description() {
        return "Read data .json file with Jackson";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getDomainEndpoint().readDataJacksonJson(bootstrap.getSession());
    }

}
