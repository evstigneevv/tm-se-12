package ru.evstigneev.tm.command.data;

import ru.evstigneev.tm.command.AbstractCommand;

public class DataWriteJaxbXmlCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA WRITE JAXB";
    }

    @Override
    public String description() {
        return "Save data to xml file";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getDomainEndpoint().writeDataJaxbXml(bootstrap.getSession());
    }

}
