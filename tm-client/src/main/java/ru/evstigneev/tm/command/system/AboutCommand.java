package ru.evstigneev.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.evstigneev.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String command() {
        return "ABOUT";
    }

    @Override
    public String description() {
        return "Show information about build";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ABOUT");
        System.out.println("Build information: ");
        System.out.println("Manifest-Version: " + Manifests.read("Manifest-Version"));
        System.out.println("Build-Number: " + Manifests.read("Build-Number"));
        System.out.println("Main-Class: " + Manifests.read("Main-Class"));
        System.out.println("Build-Jdk-Spec: " + Manifests.read("Build-Jdk-Spec"));
        System.out.println("Developer: " + Manifests.read("Developer"));
        System.out.println();
    }

}
