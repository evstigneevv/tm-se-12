package ru.evstigneev.tm.command.data;

import ru.evstigneev.tm.command.AbstractCommand;

public class DataReadJaxbXmlCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA READ JAXB";
    }

    @Override
    public String description() {
        return "Read data from xml file";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getDomainEndpoint().readDataJaxbXml(bootstrap.getSession());
    }

}
