package ru.evstigneev.tm.command.data;

import ru.evstigneev.tm.command.AbstractCommand;

public class DataWriteJacksonXmlCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA WRITE JACKSON XML";
    }

    @Override
    public String description() {
        return "Write data to .xml file with Jackson";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getDomainEndpoint().writeDataJacksonXml(bootstrap.getSession());
    }

}
